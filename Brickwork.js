/*��������� ���������

��������� ��������� � ���������, ��� ������ ��� ������ �� ����������.
������ ����� ���� ����� �� N �������� ������ (���� ���������� ��������)
���������� ���������� ����� �������� X �� Y.
����� �������� ������� ���������� �������� ������� �����.
������������ vanilla js � Html5. ������� ���� ���������� �� ���� ����������.


�������

���������� ���������:
paramX - ���������� �������� � ������� ����
paramY - ���������� �����
paramN - ���������� ������

������� � url � ������� /BrickWork.html?paramX=11&paramY=22&paramN=33
��� �������� ���������� ���������� �������� �� ��������� X = 6, Y = 6, N = 3.

*/


// ������������ ��� �������� �� ��������� �����
function Specification() {
    this.brickworkWidth = 6;
    this.brickworkHeight = 6;
    this.colorsCount = 3;
}

// ����� ������ ��� �������� �����
function Dyes() {
}

function Brick() {
    this.width = 250;
    this.height = 65;
}

// ����������� Worker. Worker - ��� �������, ������� ������ �����.
function Worker() {
    this.service = new UtilityService();
    this.specs = new Specification();
    this.dyes = new Dyes();
}

Worker.prototype.readSpecifications = function () {
    this.spec = new Specification();

    var x = this.service.getUrlParameterByName("paramX");
    if (!isNaN(parseInt(x)))
        this.specs.brickworkWidth = x;

    var y = this.service.getUrlParameterByName("paramY");
    if (!isNaN(parseInt(y)))
        this.specs.brickworkHeight = y;

    var n = this.service.getUrlParameterByName("paramN");
    if (!isNaN(parseInt(n)))
        this.specs.colorsCount = n;
}

Worker.prototype.makeDyes = function () {
    for (var i = 0; i < this.specs.colorsCount; i++) {
        var color = this.service.getRandomColor();
        this.dyes[color] = 0;
    }
}

Worker.prototype.createBrickwork = function () {
    this.readSpecifications();
    this.makeDyes();

    var canvas = document.getElementById("brickworkCanvas");
    var context = canvas.getContext("2d");

    var brick = new Brick();
    canvas.width = this.specs.brickworkWidth * brick.width;
    canvas.height = this.specs.brickworkHeight * brick.height;

    for (var j = 0; j < this.specs.brickworkHeight; j++) {
        var brickPositionY = j * brick.height;
        if (this.service.isOdd(j)) {
            // ���� ��� ��������, ������� ��� ���������� :)
            this.putBrick(brick, -(brick.width / 2), brickPositionY, context);
        }
        for (var i = 0; i < this.specs.brickworkWidth; i++) {
            var brickPositionX = i * brick.width + (this.service.isOdd(j) ? brick.width / 2 : 0);
            this.putBrick(brick, brickPositionX, brickPositionY, context);
        }
    }

    this.displayOperationReport();
}

// �� ��������� ������������� ������� ������ ���������� � ���������� ��������������� ����������
Worker.prototype.displayOperationReport = function () {
    var body = document.body, table = document.createElement("table");

    for (var color in this.dyes) {
        if (this.dyes.hasOwnProperty(color)) {
            if (this.dyes.hasOwnProperty(color)) {
                var tableRow = table.insertRow();
                var tableDataColor = tableRow.insertCell();
                tableDataColor.appendChild(document.createTextNode(color));
                var tableDataCount = tableRow.insertCell();
                tableDataCount.appendChild(document.createTextNode(this.dyes[color]));
            }
        }
    }
    body.appendChild(table);
}

Worker.prototype.useRandomDye = function () {
    var keys = Object.keys(this.dyes);
    var randomIndex = Math.floor(Math.random() * keys.length);
    this.logDyeUsage(keys[randomIndex]);
    return keys[randomIndex];
}

Worker.prototype.logDyeUsage = function (color) {
    this.dyes[color] += 1;
}

Worker.prototype.putBrick = function (brick, positionX, positionY, context) {
    context.beginPath();
    context.rect(positionX, positionY, brick.width, brick.height);

    var color = this.useRandomDye();

    context.fillStyle = color;
    context.fill();
    context.strokeStyle = "#CAC4C3";
    context.strokeRect(positionX, positionY, brick.width, brick.height);
    context.closePath();
}

// ��������������� ������
function UtilityService() {

}

// ��� ������� ������������� �� stackoverflow
UtilityService.prototype.getUrlParameterByName = function (name) {
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

UtilityService.prototype.getRandomColor = function () {
    var allowedSymbols = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
        color += allowedSymbols[Math.floor(Math.random() * 16)];
    }
    return color;
}

UtilityService.prototype.isOdd = function (number) {
    return number % 2;
}
